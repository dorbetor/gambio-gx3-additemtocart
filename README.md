# README #

Shop Gambio GX3

### What is this repository for? ###

* Overloading the class CartActionsProcess to add the new action addItemToCart
* This action can handle parameters products_id, products_qty and redirect_url.
* Version 0.0.1

### How do I get set up? ###

* Copy folder CartActionsProcess and clear the shop module cache
* Place external HTML FORM Snipp from form-demo.html
* Adjust the parameters products_id, products_qty and redirect_url
