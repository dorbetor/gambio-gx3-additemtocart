<?php
/**
 * Created by PhpStorm.
 * User: peter
 * Date: 07.11.16
 * Time: 23:16
 *
 * Extends system/classes/shopping_cart/CartActionsProcess.inc.php
 *
 * Add new action addItemToCart
 *
 *
 * @link https://developers.gambio.de/tutorials.html?v=3.1.1.0&p=class-overloading
 * @link http://www.gambio.de/forum/threads/honeygrid-und-anpassungen.24250/
 * @link http://www.gambio.de/forum/threads/modulcenter-beispiel-in-gx3-einbinden.26706/
 *
 *
 * Demo:
 * /index.php?action=addItemToCart&products_id=7&products_qty=3&redirect_url=https://www.domain.de/?id=107
 */
class CustomizedCardActionsProcess extends CartActionsProcess {

    /**
     *
     * @param null|string $p_action
     * @return void
     */
    public function proceed($p_action = NULL) {

        if($p_action == 'addItemToCart')
        {
            $products_id = 0;
            $products_qty = 1;
            $redirect_url = FALSE;

            if(isset($_GET['products_id']) && $_GET['products_id']>0) {
                $products_id = (int)$_GET['products_id'];
            }
            if(isset($_GET['products_qty']) && $_GET['products_qty']>0) {
                $products_qty = (int)$_GET['products_qty'];
            }
            if(isset($_POST['products_id']) && $_POST['products_id']>0) {
                $products_id = (int)$_POST['products_id'];
            }
            if(isset($_POST['products_qty']) && $_POST['products_qty']>0) {
                $products_qty = (int)$_POST['products_qty'];
            }
                // redirect_url
            if(isset($_GET['redirect_url']) && strlen($_GET['redirect_url'])>10) {
                $redirect_url = filter_var($_GET['redirect_url'], FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
            }
            if(isset($_POST['redirect_url']) && strlen($_POST['redirect_url'])>10) {
                $redirect_url = filter_var($_POST['redirect_url'], FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
            }

            //die(var_dump($redirect_url));

            if($products_id>0 && $products_qty>0) {
                if($this->addItemToCart($products_id, $products_qty) && $redirect_url !== FALSE) {
                    $this->set_redirect_url($redirect_url);
                    return;
                }
            }
        }


        return parent::proceed($p_action);
    }

    /**
     *
     * @param int $products_id
     * @param int $products_qty
     * @return bool
     */
    protected function addItemToCart($products_id, $products_qty=1) {
        if (isset($this->coo_cart))
        {
            $this->coo_cart->add_cart((int)$products_id, (int)$products_qty);
            return TRUE;
        }
        return FALSE;
    }
}